from notifications.africastalking_backend import sms_backend
from users.models import User, Verification


def send_verification_code(verification, user):
    phone_number = user.phone_number

    try:
        sms_backend.send(f"Welcome to encome-expense api . \nYour {verification.code_type} code is {verification.code}", [phone_number])
        print("message sent")
    except Exception as e:
        print(f'Exception: {e}')
