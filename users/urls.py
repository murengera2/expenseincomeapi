from django.urls import path
from users.views import *

urlpatterns = [

    path('admin_register',admin_register,name='admin_register'),
    path('employee_register', employee_register, name='employee_register'),
    path('activate_account',activate_account,name='activate_account'),
    path('reset_password',reset_password,name='reset_password'),
    path('forgot_password',forgot_password,name='forgot_password'),
    path('set_password',set_password,name='set_password'),
    path('login', obtain_custom_auth_token, name='auth_token_login'),

]

