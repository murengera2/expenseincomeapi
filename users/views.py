import random
import string
from datetime import datetime, timedelta, timezone

from django.contrib.auth import logout, authenticate
from django.contrib.auth.models import Group
from django.shortcuts import render
from django.utils import timezone
from rest_framework import mixins, generics, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from users.serializers import UserSerializer
from users.utils import verification_utils
from users.models import User, Verification, _generate_code


#generate password
def generate_password():
    key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=13))
    return key
@api_view(['POST'])
@permission_classes((AllowAny,))
def employee_register(request):
    employee_grp = Group.objects.filter(name='employee').first()
    password = request.data.get('password')
    email = request.data.get('email')
    name = request.data.get('name')
    username=request.data.get('username')
    phone_number = request.data.get('phone_number')


    if not name or not password or not phone_number or not email:
        return  Response({"detail":"All field are required"},status= 400)
    user=User.objects.filter(phone_number=phone_number)|User.objects.filter(email=email)| User.objects.filter(username=username)
    print(user)
    if user:
        return  Response({"detail","User with phone number already exists"},status=409)
    if not str(phone_number).startswith('+250'):
        return  Response({"detail":"Phone number must start with +250"})

    user=User(name=name,phone_number=phone_number,username=username,email=email,is_active=False)
    user.save()
    user.groups.add(employee_grp)
    user.set_password(password)
    user.save()
    verification=Verification(user=user)
    verification.save()
    verification_utils.send_verification_code(verification,user)
    user=UserSerializer(user)
    user_data=user.data
    user_data['message']='your account has been created successffuly.An SMS was sent to your number to verify '
    return  Response(user_data,status=201)





@api_view(['POST'])
# @permission_classes((, ))
def admin_register(request):
    user = request.user
    user_group = user.groups.first()
    admin_group = Group.objects.filter(name=' admin').first()


    if user_group == admin_group :

        admin_grp = Group.objects.filter(name='admin').first()


        name = request.data.get('name')
        phone_number = request.data.get('phone_number')
        email = request.data.get('email')
        username=request.data.get('username')



        if not name or not username or not phone_number  or not email:
            return Response({"detail": "Bad request"}, status=400)

        user = User.objects.filter(phone_number=phone_number) | User.objects.filter(phone_number=phone_number) | User.objects.filter(username=username)

        if user:
            return Response({"detail": "User with phone number, NID, or email already exists."}, status=409)

        if not str(phone_number).startswith('+'):
            return Response({"detail": "Bad request"}, status=400)

        user = User(
            name=name,
            phone_number=phone_number,
            email=email,
            username=username,
            is_active=True
        )

        password = generate_password()

        user.save()
        user.groups.add(admin_grp)
        user.set_password(password)
        user.save()

        response_data = UserSerializer(user).data

        response_data['initial_password'] = password
        response_data['message'] = 'admin registered successfully'

        return Response(response_data, status=201)

    return Response({'detail': 'You are not allowed to perform such action'}, status=403)

@api_view(['POST'])
@permission_classes((AllowAny, ))
def activate_account(request):
    phone_number = request.data.get('phone_number', None)
    code = request.data.get('code')

    if not phone_number :
        return Response({"detail": "Bad request"}, status=400)

    if phone_number:
        if not str(phone_number).startswith("+"):
            return Response({"detail": "Invalid phone number. Phone number must start with +"}, status=400)

    user = User.objects.filter(phone_number=phone_number).first()

    if not user:
        return Response({"detail": "User with this phone number does not exist. Register first."}, status=404)

    verification = Verification.objects.filter(code=code, user=user, code_type='activation').last()

    if not verification:
        return Response({"detail": "Verification code is incorrect."}, status=400)

    now_time = datetime.now(timezone.utc)
    time_diff = now_time - verification.time_created

    if time_diff.total_seconds() >= 3600:
        verification.code = _generate_code()
        verification.save()

        try:
            verification_utils.send_verification_code(verification, user)
        except Exception as e:
            print(f'{e}')

        return Response({"detail": "Verification code expired. Sent another one"}, status=400)

    user.is_active = True
    user.save()

    serializer = UserSerializer(user)
    serialized_data = serializer.data
    serialized_data['message'] = 'You account has been verified and activated successfully.'

    return Response(serialized_data, status=200)





@api_view(['POST'])
# @permission_classes((IsAuthenticated, ))
def reset_password(request):
    """
    Set / Reset Password of logged in user
    """
    # Add proper login

    user = request.user
    username = request.data.get('username')
    old_password = request.data.get('old_password')
    new_password = request.data.get('new_password')

    if not username or not old_password or not new_password:
        return Response({"detail": "Bad request"}, status=400)

    auth_user = authenticate(username=username, password=old_password)

    if user != auth_user:
        return Response({"detail": "Authentication credentials provided are not correct"}, status=400)

    # Set new password
    user.set_password(new_password)
    user.save()

    # Logout
    if getattr(request, 'session', False):
        logout(request)

    if getattr(user, 'auth_token', False):
        user.auth_token.delete()

    return Response({"success": "Password reset"}, status=200)




@api_view(['POST'])
@permission_classes((AllowAny, ))
def forgot_password(request):
    phone_number = request.data.get('phone_number')


    if phone_number :
        user = User.objects.filter(phone_number=phone_number).first()
        print(user)

        if not user:
            return Response({'detail': 'User not found'}, status=404)

        verification = Verification(
            user=user,
            code_type='reset'
        )
        verification.save()


        verification_utils.send_verification_code(verification, user)

        return Response({'detail': 'Reset code sent to your phone number via SMS'}, status=200)

    return Response({'detail': 'Bad request'}, status=400)



@api_view(['POST'])
@permission_classes((AllowAny, ))
def set_password(request):
    phone_number = request.data.get('phone_number')
    code = request.data.get('code')
    password = request.data.get('password')

    if not phone_number or not code or not password:
        return Response({'detail': 'Bad request'}, status=400)

    user = User.objects.filter(phone_number=phone_number).first()
    if not user:
        return Response({'detail': 'Phone number not recognized'}, status=404)

    verification = Verification.objects.filter(user=user, code=code, code_type='reset').first()
    if not verification:
        return Response({'detail': 'Verification code not recognized'}, status=404)

    now_time = datetime.now(timezone.utc)
    time_diff = now_time - verification.time_created

    if time_diff.total_seconds() >3600:
        verification.code = _generate_code()
        verification.save()
        verification_utils.send_verification_code(verification, user)

        return Response({'detail': 'Verification code has expired. Sent another one'}, status=400)

    user.set_password(password)
    user.save()

    # Logout
    if getattr(request, 'session', False):
        logout(request)

    if getattr(user, 'auth_token', False):
        user.auth_token.delete()

    return Response({'detail': 'Password reset successfully'}, status=200)


#login
class ObtainTokenWithCustomData(ObtainAuthToken):
    """
    Extends token to allow returning custom data
    about the authenticated user
    """

    def post(self, request, *args, **kwargs):
        """
        Accepts username and password and returns auth token.

        **username**: This can be a email or a phone_number
        **password**: Password of the user.

        Returns a json document containing two keys,
        **token**: Used for signing future requests
        **groups**: Shows the group which a user belong to.
        """
        serializer = AuthTokenSerializer(data=request.data)

        if serializer.is_valid():
            user = serializer.validated_data['user']
            token, _ = Token.objects.get_or_create(user=user)

            data = {
                'id': user.id,
                'name': user.name,
                'username': user.username,
                'groups': None,
                'phone_number': user.phone_number,
                'email': user.email,
                'token': token.key,
            }

            user_group = user.groups.first()

            groups = []

            if user_group is not None:
                groups.append(user_group.name)
                data['groups'] = groups

            admin_grp = Group.objects.filter(name='admin').first()

            if user_group == admin_grp:
                user = User.objects.filter(user=user.id).first()

                if user:
                    user = UserSerializer(user).data
                    data['user'] = user

            if user.is_superuser:
                data['is_superuser'] = True

            if user.is_staff:
                data['is_staff'] = True

            return Response(data, status=200)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


obtain_custom_auth_token = ObtainTokenWithCustomData.as_view()

