from  django.urls import path

from . import  views

urlpatterns=[
    path('expense/',views.ExpenseListAPIView.as_view(),name="expenses"),
    path('expense/<int:id>', views.ExpenseDetailAPIView.as_view(), name="expenses"),
]